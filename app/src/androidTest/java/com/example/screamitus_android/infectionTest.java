package com.example.screamitus_android;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class infectionTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.screamitus_android", appContext.getPackageName());
    }
    @Rule
    public ActivityTestRule activityRule =
            new ActivityTestRule<>(MainActivity.class);
    @Test
    public void testGoingToNextPage() throws InterruptedException {
        // 1. find the editText daysTextBox
        // 2. check if it is visible
        onView(withId(R.id.daysTextBox)).check(matches(isDisplayed()));

        Thread.sleep(5000);

        // 3. find the Button calculate
        //4.check if it is visible
        onView(withText("Calculate")).check(matches(isDisplayed()));
        Thread.sleep(3000);
        // 4. check if the TextView with the id resultslabel is hidden
        onView(withId(R.id.resultsLabel)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE)));

        Thread.sleep(5000);
    }
    @Test
    public void testInputBox() throws InterruptedException {
        // 1. Find the text box
        // 2. Type some number into the text box
        onView(withId(R.id.daysTextBox))
                .perform(typeText("3"));

        Thread.sleep(2000);

        // 3. find the calculate button
        //  - push the button
        onView(withText("Calculate")).perform(click());

        Thread.sleep(2000);
        // 4. Check if the result is correct
      // - expectedOutput = result
        String expectedOutput = "15";
        onView(withId(R.id.resultsLabel)).check(matches(withText(expectedOutput)));
        Thread.sleep(2000);


    }

}
